import React, { Component } from 'react';
import './App.css';
import Page from './components/Availability/Page';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Page />
            </div>
        );
    }
}

export default App;
