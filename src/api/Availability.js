const API_URL = 'http://api.ntstage.com/v1/bookingsweb/';

export default {
    getAvailable(searchDate) {
        const data = fetch(
            API_URL +
                'availability?searchDate=' +
                searchDate +
                '&routeId=4&amountResultsAfter=100&amountResultsBefore=0'
        ).then(response => response.json());

        return data;
    }
};
