import React from 'react';

const Category = props => (
    <div class="item_category">
        <p>{props.title}</p>
    </div>
);

export default Category;
