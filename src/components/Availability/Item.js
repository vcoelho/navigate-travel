import React, { Component } from 'react';

class Item extends Component {
    state = {
        products: {}
    };

    statusClass(status) {
        return status.replace(' ', '_').toLowerCase();
    }

    componentWillMount() {
        const categories = this.props.categories;
        const products = this.props.data.products;

        let categoryData = {};
        Object.keys(categories).forEach((id, key) => {
            let data = null;

            Object.keys(products).forEach((k, v) => {
                if (products[k].productClassId === id) {
                    data = products[k];
                }
            });

            categoryData[id] = data;
        });

        this.setState({
            products: categoryData
        });
    }

    render() {
        const { data } = this.props;

        const products = this.state.products;

        data.products.sort((a, b) => {
            return a.productClassId - b.productClassId;
        });

        return (
            <div className="boat_item">
                <p className="item_date">
                    <span className="item_date_text">{data.dateText}</span>
                    <span className="item_date_day">{data.dateday}</span>
                </p>
                <p className="item_temperature">{data.temperature}</p>

                {Object.keys(products).map((k, v) => {
                    if (products[k] === null) {
                        return (
                            <div key={k} className="item_product">
                                <div className="item_product_invalid">-</div>
                            </div>
                        );
                    } else {
                        const price = products[k].prices[0];
                        return (
                            <div key={k} className="item_product">
                                <div class="item_product_prices">
                                    <p className="item_product_old_price">
                                        {price.currencySymbol}
                                        {price.rrp}
                                    </p>
                                    <p className="item_product_price">
                                        {price.currencySymbol}
                                        {price.rrpWithDiscount}
                                    </p>
                                </div>
                                <p
                                    className={[
                                        'item_product_status',
                                        this.statusClass(products[k].status)
                                    ].join(' ')}
                                >
                                    {products[k].status}
                                </p>
                            </div>
                        );
                    }
                })}
            </div>
        );
    }
}

export default Item;
