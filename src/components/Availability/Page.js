import React, { Component } from 'react';
import Availability from '../../api/Availability';
import Category from './Category';
import Item from './Item';

class Page extends Component {
    state = {
        data: {},
        categories: {},
        currentDate: new Date().toISOString().split('T')[0]
    };

    renderListByDate = () => {
        Availability.getAvailable(this.state.currentDate).then(res =>
            this.setState({ data: res.data })
        );
    };

    processCategories = data => {
        let categories = {};
        Object.keys(data).forEach(k =>
            data[k].products.forEach((val, key) => {
                categories[val.productClassId] = val.productClass;
            })
        );
        return categories;
    };

    handleDateChange = e => {
        this.setState({ currentDate: e.target.value });
    };

    componentWillMount() {
        /* Loads data into state */
        Availability.getAvailable(this.state.currentDate).then(res => {
            this.setState({
                data: res.data,
                categories: this.processCategories(res.data)
            });
        });
    }

    render() {
        return (
            <div className="container">
                <h1>Navigate Travel</h1>
                <div className="boat_availability">
                    <div className="boat_categories">
                        <div className="searchInput">
                            <label htmlFor="">
                                Search for available boats on
                            </label>
                            <input
                                type="date"
                                onChange={e => this.handleDateChange(e)}
                                defaultValue={this.state.currentDate}
                            />
                            <button onClick={() => this.renderListByDate()}>
                                Search
                            </button>
                        </div>
                        {Object.keys(this.state.categories).map(k => {
                            return (
                                <Category
                                    key={k}
                                    title={this.state.categories[k]}
                                />
                            );
                        })}
                    </div>

                    <div className="boat_list">
                        {Object.keys(this.state.data).map(k => {
                            return (
                                <Item
                                    key={k}
                                    data={this.state.data[k]}
                                    categories={this.state.categories}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default Page;
